import UIKit
import SwiftUI

class MainViewController: UIViewController {
	let button1 = UIButton()
	let button2 = UIButton()
	let button3 = UIButton()
	let buttonsStack = UIStackView()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		view.backgroundColor = .white
		
		[button1, button2, button3].forEach {
			$0.translatesAutoresizingMaskIntoConstraints = false
			buttonsStack.addArrangedSubview($0)
		}
		
		buttonsStack.translatesAutoresizingMaskIntoConstraints = false
		view.addSubview(buttonsStack)
		
		button1.addTarget(self, action: #selector(openUIKit), for: .touchUpInside)
		button1.backgroundColor = .red
		button1.setTitle("UIKit", for: .normal)
		button2.addTarget(self, action: #selector(openSwiftUI), for: .touchUpInside)
		button2.backgroundColor = .green
		button2.setTitle("SwiftUI", for: .normal)
		button3.addTarget(self, action: #selector(openMixed), for: .touchUpInside)
		button3.backgroundColor = .blue
		button3.setTitle("Mixed", for: .normal)
		
		NSLayoutConstraint.activate([
			buttonsStack.centerXAnchor.constraint(equalTo: view.centerXAnchor),
			buttonsStack.centerYAnchor.constraint(equalTo: view.centerYAnchor),
		])
	}
	
	@objc func openUIKit() {
		navigationController?.pushViewController(ViewController(viewModel: ViewModel()), animated: true)
	}
	
	@objc func openSwiftUI() {
		navigationController?.pushViewController(UIHostingController(rootView: SwiftUIList(viewModel: ViewModel())), animated: true)
	}
	
	@objc func openMixed() {
		navigationController?.pushViewController(UIHostingController(rootView: SwiftUIWithUIKit(viewModel: ViewModel())), animated: true)
	}
}
