//
//  ViewController.swift
//  SwiftUIInUIKit
//
//  Created by Martin Štrambach on 18.11.2021.
//

import UIKit
import Combine

class ViewController: UIViewController {
	let tableView = UITableView()
	let label = UILabel()
	let viewModel: ViewModel
	var cancelable: AnyCancellable?
	
	init(viewModel: ViewModel) {
		self.viewModel = viewModel
		super.init(nibName: nil, bundle: nil)
		
		self.cancelable = self.viewModel.objectWillChange.sink {
			self.label.text = String(self.viewModel.count)
			self.tableView.reloadData()
		}
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		view.backgroundColor = .white
		
		label.translatesAutoresizingMaskIntoConstraints = false
		label.text = "view"
		label.textAlignment = .center
		view.addSubview(label)
		
		tableView.translatesAutoresizingMaskIntoConstraints = false
		tableView.delegate = self
		tableView.dataSource = self
		view.addSubview(tableView)
		tableView.register(TableViewCell.self, forCellReuseIdentifier: TableViewCell.reuseIdentifier)
		
		NSLayoutConstraint.activate([
			label.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
			label.leadingAnchor.constraint(equalTo: view.leadingAnchor),
			label.trailingAnchor.constraint(equalTo: view.trailingAnchor),
			
			tableView.topAnchor.constraint(equalTo: label.bottomAnchor),
			tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
			tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
			tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
		])
	}
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		viewModel.elements
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = TableViewCell()
		cell.imageView1.image = viewModel.randomImage
		cell.label1.text = "title: \(indexPath.row + viewModel.count)"
		cell.label2.text = "subtitle: \(indexPath.row - viewModel.count)"
		cell.imageView2.image = viewModel.randomImage
		return cell
	}
}

