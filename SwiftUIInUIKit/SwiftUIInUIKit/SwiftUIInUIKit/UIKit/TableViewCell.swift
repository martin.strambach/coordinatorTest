import UIKit

class TableViewCell: UITableViewCell {
	let label1 = UILabel()
	let label2 = UILabel()
	let imageView1 = UIImageView()
	let imageView2 = UIImageView()

	static let reuseIdentifier = "TableViewCell"
	
	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		
		[label1, label2, imageView1, imageView2].forEach {
			addSubview($0)
			$0.translatesAutoresizingMaskIntoConstraints = false
		}
		
		imageView1.contentMode = .scaleAspectFit
		imageView2.contentMode = .scaleAspectFit
		
		activateConstraints()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	func activateConstraints() {
		NSLayoutConstraint.activate([
			imageView1.topAnchor.constraint(equalTo: topAnchor, constant: 5),
			imageView1.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			imageView1.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -5),
			imageView1.widthAnchor.constraint(equalToConstant: 30),
			
			label1.topAnchor.constraint(equalTo: topAnchor, constant: 5),
			label1.leadingAnchor.constraint(equalTo: imageView1.trailingAnchor, constant: 20),
			
			label2.topAnchor.constraint(equalTo: label1.bottomAnchor, constant: 5),
			label2.leadingAnchor.constraint(equalTo: imageView1.trailingAnchor, constant: 20),
			label2.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -5),
			
			imageView2.topAnchor.constraint(equalTo: topAnchor, constant: 5),
			imageView2.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
			imageView2.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -5),
		])
	}
}
