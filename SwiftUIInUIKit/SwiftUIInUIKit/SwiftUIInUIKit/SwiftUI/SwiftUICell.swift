//
//  SwiftUICell.swift
//  SwiftUIInUIKit
//
//  Created by Martin Štrambach on 18.11.2021.
//

import SwiftUI

struct SwiftUICell: View {
	let image1: UIImage
	let image2: UIImage
	let number1: Int
	let number2: Int
	
	var body: some View {
		HStack {
			Image(uiImage: image1)
				.resizable()
				.aspectRatio(contentMode: .fit)
				.frame(width: 30)
			Spacer()
				.frame(width: 20)
			VStack(alignment: .leading) {
				Text("title: \(number1)")
				Text("subtitle: \(number2)")
			}
			Spacer()
			Image(uiImage: image2)

		}
	}
}

struct SwiftUICell_Previews: PreviewProvider {
	static var previews: some View {
		SwiftUICell(image1: UIImage(systemName: "trash")!, image2: UIImage(systemName: "trash")!, number1: 1, number2: 2)
	}
}
