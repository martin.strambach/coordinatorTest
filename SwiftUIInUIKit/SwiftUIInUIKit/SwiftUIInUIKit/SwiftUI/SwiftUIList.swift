//
//  SwiftUICell.swift
//  SwiftUIInUIKit
//
//  Created by Martin Štrambach on 18.11.2021.
//

import SwiftUI

struct SwiftUIList: View {
	@ObservedObject var viewModel: ViewModel
	
	var body: some View {
		List(1..<viewModel.elements) { item in
			SwiftUICell(
				image1: viewModel.randomImage ?? UIImage(),
				image2: viewModel.randomImage ?? UIImage(),
				number1: item + viewModel.count,
				number2: item - viewModel.count
			)
		}
	}
}

struct SwiftUIList_Previews: PreviewProvider {
	static var previews: some View {
		SwiftUIList(viewModel: ViewModel())
	}
}
