import SwiftUI

struct TableViewCellSwiftUI: UIViewRepresentable {
	let number1: Int
	let number2: Int
	let image1: UIImage
	let image2: UIImage
	
	func updateUIView(_ uiView: TableViewCell, context: Context) {
		uiView.label1.text = "title: \(number1)"
		uiView.label2.text = "subtitle: \(number2)"
		uiView.imageView1.image = image1
		uiView.imageView2.image = image2
	}


	func makeUIView(context: Context) -> TableViewCell {
		let cell = TableViewCell()
		cell.label1.text = "title: \(number1)"
		cell.label2.text = "subtitle: \(number2)"
		cell.imageView1.image = image1
		cell.imageView2.image = image2
		return cell
	}
}
