//
//  SwiftUICell.swift
//  SwiftUIInUIKit
//
//  Created by Martin Štrambach on 18.11.2021.
//

import SwiftUI

struct SwiftUIWithUIKit: View {
	@ObservedObject var viewModel: ViewModel
	
	var body: some View {
		List(1..<viewModel.elements) { item in
			TableViewCellSwiftUI(
				number1: item + viewModel.count,
				number2: item - viewModel.count,
				image1: viewModel.randomImage ?? UIImage(),
				image2: viewModel.randomImage ?? UIImage()
			)
		}
	}
}

struct SwiftUIWithUIKit_Previews: PreviewProvider {
	static var previews: some View {
		SwiftUIWithUIKit(viewModel: ViewModel())
	}
}
