import Combine
import Foundation
import UIKit

class ViewModel: ObservableObject {
	var timer: AnyCancellable?
	@Published var count = 1
	let elements: Int = 1000
	let images = [UIImage(systemName: "trash"), UIImage(systemName: "archivebox.fill"), UIImage(systemName: "calendar.badge.plus"), UIImage(systemName: "tram.fill"), UIImage(systemName: "car.2.fill")]
	
	var randomImage: UIImage? {
		(images.randomElement())!
	}
	
	init() {
		self.timer = Timer.publish(every: 1, on: RunLoop.main, in: .common)
		  .autoconnect()
		  .sink { [unowned self] _ in
			  count += 1
		  }
	}
}
