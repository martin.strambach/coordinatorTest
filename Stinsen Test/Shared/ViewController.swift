import UIKit
import SwiftUI

final class ViewController: UIViewController {
	let label = UILabel()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		view.addSubview(label)
		label.translatesAutoresizingMaskIntoConstraints = false
		label.text = "View controller"
		view.centerXAnchor.constraint(equalTo: label.centerXAnchor).isActive = true
		view.centerYAnchor.constraint(equalTo: label.centerYAnchor).isActive = true
	}
}

struct ViewControlerView: UIViewControllerRepresentable {
	func updateUIViewController(_ uiViewController: ViewController, context: Context) {
		
	}
	
	typealias UIViewControllerType = ViewController
	func makeUIViewController(context: UIViewControllerRepresentableContext<ViewControlerView>) -> ViewController {
		ViewController()
	}
}
