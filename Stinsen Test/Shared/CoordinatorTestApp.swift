import SwiftUI
import Stinsen
import Resolver

@main
struct CoordinatorTestApp: App {
	@UIApplicationDelegateAdaptor(AppDelegate.self) var appDelegate
	
    var body: some Scene {
        WindowGroup {
			NavigationViewCoordinator(Resolver.resolve(MainCoordinator.self)).view()
        }
    }
	
	
}
