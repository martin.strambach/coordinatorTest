import Stinsen
import SwiftUI

final class Coordinator2: NavigationCoordinatable {
	let stack = NavigationStack(initial: \Coordinator2.start)
	
	@Root var start = makeStart
	@Route(.push) var view2push = view2
	
	@ViewBuilder func makeStart() -> some View {
		View3()
	}
	
	@ViewBuilder func view2() -> some View {
		View2()
	}
}
