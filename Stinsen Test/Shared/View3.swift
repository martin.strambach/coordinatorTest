//
//  View2.swift
//  CoordinatorTest
//
//  Created by Martin Štrambach on 16.11.2021.
//

import SwiftUI

struct View3: View {
	
	@EnvironmentObject var router: Coordinator2.Router
	
	var body: some View {
		VStack {
			Text("View3")
			Button("view 2 push") {
				router.route(to: \.view2push)
			}
		}
	}
}

struct View3_Previews: PreviewProvider {
	static var previews: some View {
		View3()
	}
}
