import UIKit
import Resolver

class AppDelegate: NSObject, UIApplicationDelegate {
	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
		Resolver.register { MainCoordinator() }.scope(.application)
		return true
	}
}
