//
//  ContentView.swift
//  Shared
//
//  Created by Martin Štrambach on 16.11.2021.
//

import SwiftUI

struct ContentView: View {
	@EnvironmentObject var router: MainCoordinator.Router
	
    var body: some View {
		VStack {
			Text("View 1")
				.padding()
			Button("push") {
				router.route(to: \.view2)
			}
			Button("modal") {
				router.route(to: \.view2Modal)
			}
			Button("coordinator") {
				router.route(to: \.coordinatorPush)
			}
			Button("UIKit VC") {
				router.route(to: \.uikitVCPush)
			}
		}
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
