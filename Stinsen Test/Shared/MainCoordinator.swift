import Stinsen
import SwiftUI

final class MainCoordinator: NavigationCoordinatable {
	let stack = NavigationStack(initial: \MainCoordinator.start)
	
	@Root var start = makeStart
	@Route(.push) var view2 = makeView2
	@Route(.modal) var view2Modal = makeView2
	@Route(.push) var coordinatorPush = coordinator
	@Route(.push) var uikitVCPush = uikitVC
	
	@ViewBuilder func makeStart() -> some View {
		ContentView()
	}
	
	@ViewBuilder func makeView2() -> some View {
		View2()
	}
	
	func coordinator() -> Coordinator2 {
		Coordinator2()
	}
	
	@ViewBuilder func uikitVC() -> some View {
		ViewControlerView()
	}
}
