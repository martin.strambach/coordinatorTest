//
//  View2.swift
//  CoordinatorTest
//
//  Created by Martin Štrambach on 16.11.2021.
//

import SwiftUI
import Stinsen
import Resolver

struct View2: View {
	var router1: MainCoordinator.Router? = RouterStore.shared.retrieve()
	@EnvironmentObject var router2: Coordinator2.Router
	@Injected var mainCoordinator: MainCoordinator
	
    var body: some View {
		VStack {
			Text("View2")
			Button("pop all main coordinator") {
//				router1?.popToRoot()
				_ = mainCoordinator.popToRoot()
			}
			Button("pop all coordinator 2") {
				router2.popToRoot()
			}
			Button("dismiss") {
				router2.dismissCoordinator()
			}
		}
    }
}

struct View2_Previews: PreviewProvider {
    static var previews: some View {
        View2()
    }
}
